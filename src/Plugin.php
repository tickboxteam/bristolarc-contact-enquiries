<?php 
namespace Bristolarc\Contacts;

use Hummingbird\Classes\PluginBase;
use Setting;

/**
 * Plugin class for handling plugin specific functions
 *
 * @package WWT\Blog
 * @author Daryl Phillips
 *
 */

class Plugin extends PluginBase {
	public function __construct($package_details = array()) {
		$this->package = $package_details;
	}
}