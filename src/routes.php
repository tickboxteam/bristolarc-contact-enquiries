<?php 
Route::group(array('before' => 'install.before'), function() {

    Route::post('about/contact-us/validate', ['as' => 'BristolArc.Complaints.Validate', 'uses' => 'Bristolarc\Contacts\Controllers\ContactsFrontendController@complaintsFormValidate']);
});
