<?php namespace Bristolarc\Contacts\Controllers;

use FrontendController;
use Input;
use Log;
use Mail;
use Redirect;
use Request;
use Validator;

/**
 * 
 *
 * @author  Daryl Phillips <darylp@tickboxmarketing.co.uk>
 * @version 1.0.0 <Base installation version without any fancy features>
 * @package Tickbox\Contacts
 */
class ContactsFrontendController extends FrontendController
{
    public $handles_children    = true;
    
    /**
     * Main constructor for initialising CMS features
     */
    public function __construct($attributes = []) {
        parent::__construct($attributes);
    }

    /**
     * Validate Contact Form
     * @return Mixed
     */
    public function complaintsFormValidate() {
        $rules = [
            'name'      => 'required',
            'phone'     => 'required|phone',
            'email'     => 'required|email',
            'complaint' => 'required'
        ];

        $messages = [
            'name.required'     => 'Please enter your name',
            'phone.required'    => 'Please enter your contact number',
            'phone.phone'       => 'Please enter a valid contact number',    
            'email.required'    => 'Please enter your email address',
            'email.email'       => 'Please enter a valid email address',
            'complaint.required' => 'Please enter your complaint'
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);

        if(!$validator->passes()) {
            return Redirect::back()
                ->with('error', 'Please review your input')
                ->withErrors($validator)
                ->withInput();
        }
        
        Mail::send('theme::emails.complaints-notification', Input::all(), function($message) {
            $message->to('info@bristolarc.org.uk')
                ->subject('New complaint received by website');
        });

        return Redirect::back()
            ->with('complaint-form-success', 'Your message has been successfully sent');
    }
}
