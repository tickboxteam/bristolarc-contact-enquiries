<?php namespace Bristolarc\Contacts;

use Illuminate\Support\ServiceProvider;
use File;
use Validator;
use View;

class ContactsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot() {}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		// routes file
		if(File::exists(__DIR__ . '/routes.php'))
			include __DIR__ . '/routes.php';
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides() {
		return array();
	}
}
